from django.db import models

# Create your models here.
class GeoAPI(models.Model):
    country = models.CharField(max_length=250, blank=True, default='', verbose_name='País')
    name = models.CharField(max_length=250, blank=True, default='', verbose_name='Ciudad')
    weather = models.CharField(max_length=250, blank=True, default='', verbose_name='Clima')
    description = models.CharField(max_length=250, blank=True, default='', verbose_name='Descripción Clima')

    temp = models.DecimalField(default=0.00, max_digits=7, blank=True,decimal_places=2, verbose_name='Temperatura')
    feels_like = models.DecimalField(default=0.00, max_digits=7, blank=True,decimal_places=2, verbose_name='Sensación Térmica')
    temp_min = models.DecimalField(default=0.00, max_digits=7, blank=True,decimal_places=2, verbose_name='Temperatura Mínima')
    temp_max = models.DecimalField(default=0.00, max_digits=7, blank=True,decimal_places=2, verbose_name='Temperatura Máxima')
    pressure = models.IntegerField(default=0, blank=True, verbose_name='Presión')
    humidity = models.IntegerField(default=0, blank=True, verbose_name='Humedad')
    sea_level = models.IntegerField(default=0, blank=True, verbose_name='Presión Atmosférica Nivel Mar')
    grnd_level = models.IntegerField(default=0, blank=True, verbose_name='Presión Atmosférica Nivel Tierra')
    visibility = models.IntegerField(default=0, blank=True, verbose_name='Visibilidad')
    speed = models.DecimalField(default=0.00, max_digits=7, blank=True, decimal_places=2, verbose_name='Velocidad Viento')
    deg = models.DecimalField(default=0.00, max_digits=7, blank=True, decimal_places=2, verbose_name='Dirección Viento')
    gust = models.DecimalField(default=0.00, max_digits=7, blank=True, decimal_places=2, verbose_name='Ráfaga Viento')
    all = models.IntegerField(default=0, blank=True, verbose_name='nubosidad')


    def __str__(self):
        return self.name


    class Meta:
        db_table = 'geoapi'
        ordering = ['id']