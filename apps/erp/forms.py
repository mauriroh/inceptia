from django.forms import ModelForm
from apps.erp.models import *

####################################
#              GeoAPI              #
####################################
class GeoAPIForm(ModelForm):

    class Meta:
        model = GeoAPI
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data