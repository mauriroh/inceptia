from django.contrib import admin

from apps.erp.models import *

# Register your models here.
class ModelAdminGeoAPI(admin.ModelAdmin):
    ordering = ['id']
admin.site.register(GeoAPI, ModelAdminGeoAPI)