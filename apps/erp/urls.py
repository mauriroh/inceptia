from django.urls import path
from apps.erp.views import *

app_name = 'app'

urlpatterns = [
    path('geoapi/',GeoAPI),
    path('stock/', Stock),
    path('discount-code/', DiscountCode),
]