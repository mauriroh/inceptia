import pandas as pd
from django.http import HttpResponse
import requests
from collections import Counter

API_key = "d81015613923e3e435231f2740d5610b"
lat = "-35.836948753554054"
lon = "-61.870523905384076"
# lat = "44.34"
# lon = "10.99"
URLAPI = 'https://api.openweathermap.org/data/2.5/weather?lat='f'{lat}''&lon='f'{lon}''&appid='f'{API_key}'

#
####################################
#           Ejercicio 1:           #
####################################
# class GeoAPI:
#     API_KEY = "d81015613923e3e435231f2740d5610b"
#     LAT = "-35.836948753554054"
#     LON = "-61.870523905384076"
#     @classmethod
#     def is_hot_in_pehuajo(cls):
#         'lat': f'{cls.LAT}',
#         'lon': f'{cls.LON}',
#         'API_key': f'{cls.API_KEY}'
#         URLAPI = requests.get('https://api.openweathermap.org/data/2.5/weather?lat='f'{lat}''&lon='f'{lon}''&appid='f'{API_key}')

def GeoAPI(request):

    def is_get_api():
        response = requests.get(URLAPI)
        if response.status_code == 200:
            response_son = response.json()
            tempKelvin = response_son['main']['temp']
            tempCelsius = tempKelvin - 273.15
            tempKelvinMin = response_son['main']['temp_min']
            tempCelsiusMin = tempKelvinMin - 273.15
            tempKelvinMax = response_son['main']['temp_max']
            tempCelsiusMax = tempKelvinMax - 273.15

            try:
                if tempCelsius > 28:
                    # Data Extra que la usé para corroborar
                    print('***** TRUE ******')
                    print('***** Temperatura SUPERIOR a los 28° C ******')
                    print('Country: ')
                    print(response_son['sys']['country'])
                    print('City Name')
                    print(response_son['name'])
                    print('Weather Conditions: ')
                    print('Temperature: C°')
                    print(tempCelsius)
                    print('Feels Like: C°')
                    print(response_son['main']['feels_like'])
                    print('Minimum Temperature: C°')
                    print(tempCelsiusMin)
                    print('Maximum Temperature: C°')
                    print(tempCelsiusMax)
                    print('Atmospheric Pressure: hPa')
                    print(response_son['main']['pressure'])
                    print('Humidity: %')
                    print(response_son['main']['humidity'])
                    print('Atmospheric Pressure on the Sea Level: hPa')
                    print(response_son['main']['sea_level'])
                    print('Atmospheric Pressure on the Ground Level: hPa')
                    print(response_son['main']['grnd_level'])
                    print('Visibility (meter): ')
                    print(response_son['visibility'])
                    print('Wind Speed (meter/sec): ')
                    print(response_son['wind']['speed'])
                    print('Wind direction (degrees): ')
                    print(response_son['wind']['deg'])
                    print('Wind gust (meter/sec): ')
                    print(response_son['wind']['gust'])
                    print('Cloudiness: %')
                    print(response_son['clouds']['all'])

                    # Respuesta requerida
                    # print('TRUE - Temperatura SUPERIOR a los 28° C')
                    return True

                else:
                    # Data Extra que la usé para corroborar
                    print('***** FALSE ******')
                    print('***** Temperatura INFERIOR a los 28° C ******')
                    print('Country: ')
                    print(response_son['sys']['country'])
                    print('City Name')
                    print(response_son['name'])
                    print('Weather Conditions: ')
                    print('Temperature: C°')
                    print(tempCelsius)
                    print('Feels Like: C°')
                    print(response_son['main']['feels_like'])
                    print('Minimum Temperature: C°')
                    print(tempCelsiusMin)
                    print('Maximum Temperature: C°')
                    print(tempCelsiusMax)
                    print('Atmospheric Pressure: hPa')
                    print(response_son['main']['pressure'])
                    print('Humidity: %')
                    print(response_son['main']['humidity'])
                    print('Atmospheric Pressure on the Sea Level: hPa')
                    print(response_son['main']['sea_level'])
                    print('Atmospheric Pressure on the Ground Level: hPa')
                    print(response_son['main']['grnd_level'])
                    print('Visibility (meter): ')
                    print(response_son['visibility'])
                    print('Wind Speed (meter/sec): ')
                    print(response_son['wind']['speed'])
                    print('Wind direction (degrees): ')
                    print(response_son['wind']['deg'])
                    print('Wind gust (meter/sec): ')
                    print(response_son['wind']['gust'])
                    print('Cloudiness: %')
                    print(response_son['clouds']['all'])

                    # Respuesta requerida
                    # print('FALSE - Temperatura INFERIOR a los 28° C')
                    return False

            except:
                # print('FALSE = ERROR')
                return False
            finally:
                pass

    result_GeoApi = is_get_api()
    print(result_GeoApi)
    return HttpResponse(result_GeoApi)




####################################
#            Ejercicio 2:          #
####################################
def Stock(request):

    _PRODUCT_DF = pd.DataFrame({
        "product_name": ["Chocolate", "Granizado", "Limon", "Dulce de Leche"],
        "quantity": [3, 10, 0, 5]
    })

    names = _PRODUCT_DF['product_name']
    quant = _PRODUCT_DF['quantity']
    entry = 'Granizado'

    def is_product_available(product_name, quantity, dataEntry):
        flag = False
        try:
            if not dataEntry == '':
                cont = 0
                for i in product_name:
                    if dataEntry in i:
                        if quantity[cont] > 0:
                            flag = True
                            break
                    cont += 1
                if flag == True:
                    # print('TRUE - Producto CON Stock')
                    return True
                else:
                    # print('FALSE - Producto SIN Stock')
                    return False
            else:
                # print('FALSE - El Producto NO EXISTE')
                return False
        except:
            # print('FALSE = ERROR')
            return False
        finally:
            pass

    result_PRODUCT_DF = is_product_available(names, quant, entry)
    print(result_PRODUCT_DF)
    return HttpResponse(result_PRODUCT_DF)



####################################
#            Ejercicio 3:          #
####################################
def DiscountCode(request):

    AVAILABLE_DISCOUNT_CODES = ["Primavera2021", "Verano2021", "Navidad2x1", "heladoFrozen"]
    user_code = 'Navidad2x1'

    def compare_text(text1, text2):
        contText1 = Counter(text1)
        contText2 = Counter(text2)

        common = contText1 & contText2

        return len(common)


    def validate_discount_code(discount_code, ucode):
        flag = False
        try:
            if not ucode == '':
                if ucode in discount_code:
                    flag = True
                else:
                    for i in discount_code:
                        lengTexto = compare_text(i, ucode)
                        if lengTexto == (len(i) - 1):
                            flag = True
                        else:
                            flag = False

                if flag == True:
                    # print('TRUE = Código Válido')
                    result = True
                else:
                    # print('FALSE = Código Inválido')
                    result = False
                return result

            else:
                # print('FALSE = No se ingresó ningún Código')
                return False

        except:
            # print('FALSE = ERROR')
            return False
        finally:
            pass

    result_DiscountCode = validate_discount_code(AVAILABLE_DISCOUNT_CODES, user_code)
    print(result_DiscountCode)
    return HttpResponse(result_DiscountCode)

